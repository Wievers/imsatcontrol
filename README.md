## Name
IMSAT OdB code

## Description
The code here is the code flashed into the On-Board computer of the 2024 probe balloon of the club IMSAT.
It manages:
    - The STM32 card (L152RE)
    - 2 BME280 representing an outside probe and an internal probe
    - 1 INA219 to measure the output of the solar pannels. 

## Authors and acknowledgment
This project was done by the club IMSAT and the 2024 IMSAT Project Team

## License
This project is licenced under the Public Licence GNU GPLv3
